var fs = require('fs');

var stat_object = {
        count_pending_messages: {regex: /GET path=\/api\/users\/[0-9]*\/count_pending_messages/gi, count: 0, response_time: [], dynos: []},
        get_messages: {regex: /GET path=\/api\/users\/[0-9]*\/get_messages/gi, count: 0, response_time: [], dynos: []},
        get_friends_progress: {regex: /GET path=\/api\/users\/[0-9]*\/get_friends_progress/gi, count: 0, response_time: [], dynos: []},
        get_friends_score: {regex: /GET path=\/api\/users\/[0-9]*\/get_friends_score/gi, count: 0, response_time: [], dynos: []},
        all_post: {regex: /POST path=\/api\/users\//gi, count: 0, response_time: [], dynos: []},
        all_get: {regex: /GET path=\/api\/users\//gi, count: 0, response_time: [], dynos: []}
    };

var path_to_file = process.argv[2] || './sample.log';

fs.readFile(path_to_file, {encoding:'utf-8', flag: 'r'}, function(err, data) {
    if (err) throw err;
    
    var str_temp = '', str_logs = data.split('\n');
    
    for (var i = 0; i < str_logs.length; i++) 
    {
        str_temp = str_logs[i];

        if (str_temp.match(stat_object.count_pending_messages.regex))
        {
            stat_object.count_pending_messages.count += 1;
            stat_object.count_pending_messages.response_time.push(getResponseTime(str_temp));
            stat_object.count_pending_messages.dynos.push(getDyno(str_temp));
        } 
        else if(str_temp.match(stat_object.get_messages.regex))
        {
            stat_object.get_messages.count += 1;
            stat_object.get_messages.response_time.push(getResponseTime(str_temp));
            stat_object.get_messages.dynos.push(getDyno(str_temp));
        }
        else if(str_temp.match(stat_object.get_friends_progress.regex))
        {
            stat_object.get_friends_progress.count += 1;
            stat_object.get_friends_progress.response_time.push(getResponseTime(str_temp));
            stat_object.get_friends_progress.dynos.push(getDyno(str_temp));
        }
        else if(str_temp.match(stat_object.get_friends_score.regex))
        {
            stat_object.get_friends_score.count += 1;
            stat_object.get_friends_score.response_time.push(getResponseTime(str_temp));
            stat_object.get_friends_score.dynos.push(getDyno(str_temp));
        }
        
        if (str_temp.match(stat_object.all_get.regex))
        {
            stat_object.all_get.count += 1;
            stat_object.all_get.response_time.push(getResponseTime(str_temp));
            stat_object.all_get.dynos.push(getDyno(str_temp));
        }
        else if (str_temp.match(stat_object.all_post.regex))
        {
            stat_object.all_post.count += 1;
            stat_object.all_post.response_time.push(getResponseTime(str_temp));
            stat_object.all_post.dynos.push(getDyno(str_temp));
        }
    }
    
    printStats('GET /api/users/{user_id}/count_pending_messages', stat_object.count_pending_messages);
    printStats('GET /api/users/{user_id}/get_messages', stat_object.get_messages);
    printStats('GET /api/users/{user_id}/get_friends_progress', stat_object.get_friends_progress);
    printStats('GET /api/users/{user_id}/get_friends_score', stat_object.get_friends_score);
    printStats('POST /api/users/{user_id}', stat_object.all_post);
    printStats('GET /api/users/{user_id}', stat_object.all_get);

});

function printStats(label, stat_node)
{
    console.log(label + ':',
        'times of calling=' + stat_node.count, 
        'mean=' + getMeanValue(stat_node.response_time), 
        'median=' + getMedianValue(stat_node.response_time),
        'mode=' + getModeValue(stat_node.response_time),
        'most response=' + mostOccured(stat_node.dynos));
}

function getResponseTime(str_log)
{
    var connects = str_log.match(/connect=[0-9]*/i),
        services = str_log.match(/service=[0-9]*/i),
        response_time = 0;
    
    if (typeof(connects) !== 'undefined')
        response_time = parseInt(connects[0].substr(connects[0].indexOf('=') + 1, connects[0].length - 1));
        
    if (typeof(services) !== 'undefined')
        response_time += parseInt(connects[0].substr(services[0].indexOf('=') + 1, services[0].length - 1));
        
    return response_time;
}

function getDyno(str_log) 
{
    var matches = str_log.match(/dyno=[a-zA-Z.]*[0-9]*/i);
    if (matches)
        return matches[0].substr(matches[0].indexOf('=') + 1, matches[0].length - 1);
    return null;
}

function getMeanValue(numbers)
{
    var summary = 0;
    
    for (var i = 0; i < numbers.length; i++)
        summary += numbers[i];
        
    return parseFloat(summary/numbers.length).toFixed(2);
}

function getMedianValue(numbers)
{
    numbers.sort(function(a, b) { return a-b; });
    
    var value = -1;
    
    if (numbers.length%2 == 0)
        value = numbers[Math.floor(numbers.length/2)] + numbers[Math.floor(numbers.length/2) + 1];
    else
        value = numbers[Math.floor(numbers.length/2) + 1]; 
    
    return value;
}

function getModeValue(numbers)
{
   return mostOccured(numbers);
}

function mostOccured(array)
{
    var hash = {},
        max_n = array[0],
        max_count = 1;

    for(var i = 0; i < array.length; i++)
    {
        var num = array[i];
    	
        hash[num] = (hash[num] == null)? 1 : hash[num] + 1;

        if (hash[num] > max_count)
        {
            max_n = num;
            max_count = hash[num];
        }
    }
    return max_n;
}